import java.util.Date;

import input.Command;
import utils.DateUtils;

public class DateTimePrinterMain {
    public static void main(String[] args) {
        Command<String> command = new Command<>(args);
        System.out.println(command.execute(format -> DateUtils.getCurrentDateTime(format)));
    }
}
