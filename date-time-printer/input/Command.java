package input;

import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Command<T> {
    
    private String value;

    public Command(String[] args) {
        if (args.length > 0) {
            value = Stream.of(args).collect(Collectors.joining(" "));
        } else {
            value = "u-M-d'T'H:m:s'Z'";
        }
    }

    public T execute(Function<String, T> func) {
        return func.apply(value);
    }

}