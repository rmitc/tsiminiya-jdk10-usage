package utils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public final class DateUtils {
    
    protected DateUtils() {
        throw new UnsupportedOperationException();
    }

    public static String getCurrentDateTime(String format) {
        return DateTimeFormatter.ofPattern(format).format(LocalDateTime.now());
    }

}